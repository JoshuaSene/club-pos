import {
  Module
} from '@nestjs/common';
import {
  TypeOrmModule
} from '@nestjs/typeorm';
import {
  GraphQLModule
} from '@nestjs/graphql';
import {
  join
} from 'path';

import {
  AppController
} from './app.controller';
import {
  AppService
} from './app.service';
import {
  AuthModule
} from './auth/auth.module';
import {
  RoleModule
} from './role/role.module';
 
import {
  UserModule
} from './user/user.module';
import {
  UserRoleModule
} from './user-role/user-role.module';
 
import {
  EmailConfirmationModule
} from './email-confirmation/email-confirmation.module';
import {
  EnabledConfirmationModule
} from './email-confirmation/user-confirmation.module';
import { SendDonateModule } from './send-donate/send-donate.module'; 
import { TypePfModule } from './type-pf/type-pf.module';
import { TypePjModule } from './type-pj/type-pj.module';
 
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      context: ({
        req
      }) => ({
        req
      })
    }),
    UserModule,
    AuthModule,
    RoleModule,
    UserRoleModule, 
    EmailConfirmationModule, 
    EnabledConfirmationModule, SendDonateModule,  TypePfModule, TypePjModule, 
   
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}