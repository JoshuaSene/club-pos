import {
  ExecutionContext,
  Injectable,
  CanActivate,
  UnauthorizedException
} from '@nestjs/common';
import {
  Reflector
} from '@nestjs/core';
import {
  GqlExecutionContext
} from '@nestjs/graphql';
import {
  AuthGuard
} from '@nestjs/passport';
import {
  Request
} from 'express'; 
import Role from 'src/role/entities/role.entity';
import {
  UserRole
} from 'src/user-role/entities/user-role.entity';
import {
  getRepository
} from 'typeorm';

@Injectable()
export class GqlAuthGuard extends AuthGuard('jwt') {
  constructor(private readonly reflector: Reflector) {
    super();
  }
  getRequest(context: ExecutionContext): Request {
    const ctx = GqlExecutionContext.create(context);
    context.getHandler()
    return ctx.getContext().req;
  }
}

@Injectable()
export class GuardByRoles extends AuthGuard('jwt') {
  async getRequestRole(context: ExecutionContext): Promise < any > {

    const ctx = GqlExecutionContext.create(context);
    const roleId = await ctx.getContext().req.user.userRole["roleId"]

    const userLogRole = (await getRepository(UserRole).findOne({
      roleId: `${roleId}`
    })).roleId

    const admRoleName = (await getRepository(Role).findOne({
      name: `ADM`
    })).id


    return ctx.getContext()
  }
}


@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get < string[] > ('roles', context.getHandler());

    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();

    const user = request.user;
    return user
  }
}

@Injectable()
export class GuardByRole implements CanActivate {

  async canActivate(
    context: ExecutionContext,
  ): Promise < any > {
    const ctx = GqlExecutionContext.create(context);
    const roleId = ctx.getContext().req.user.userRole["roleId"]


    const userLogRole = (await getRepository(UserRole).findOne({
      roleId: `${roleId}`
    })).roleId

    const admRoleName = (await getRepository(Role).findOne({
      name: `ADM`
    })).id

    if (userLogRole != admRoleName) {
      throw new UnauthorizedException('UnauthorizedException')
    }

    return true
  }
}


// @Injectable()
// export class ContractGuard implements CanActivate {

//   async canActivate(
//     context: ExecutionContext,
//   ): Promise < any > {
//     const ctx = GqlExecutionContext.create(context);
//     const roleId = ctx.getContext().req.user.userRole["roleId"]
//     const userLogId = ctx.getContext().req.user.userRole["userId"]

//     const contractId = ctx.getContext().req.body.query.substring(44, 80)
//     const verifyContract = (await getRepository(Contract).findOne(contractId))

//     const userLogRole = (await getRepository(UserRole).findOne({
//       roleId: `${roleId}`
//     })).roleId

//     const admRoleName = (await getRepository(Role).findOne({
//       name: `ADM`
//     })).id

//     if ((userLogRole != admRoleName) && (verifyContract.userId != userLogId)) {
//       throw new UnauthorizedException('UnauthorizedException')
//     }

//     return true
//   }
// }

@Injectable()
export class CreateContractGuard implements CanActivate {

  async canActivate(
    context: ExecutionContext,
  ): Promise < any > {
    const ctx = GqlExecutionContext.create(context);
    const userLogId = ctx.getContext().req.user.userRole["userId"]

    // const contractId = ctx.getContext().req.body.query.substring(44, 80) 
    // await getRepository(Contract).findOne(contractId)

    // getRepository(Contract).findOne(req.data.contract)
    const userLogRole = (await getRepository(UserRole).findOne({
      userId: `${userLogId}`
    })).userId

    return true
  }
}