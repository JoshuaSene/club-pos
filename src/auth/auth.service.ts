import {
  Injectable,
  UnauthorizedException
} from '@nestjs/common';
import {
  JwtService
} from '@nestjs/jwt';
import {
  UserRole
} from 'src/user-role/entities/user-role.entity';
 
import {
  getConnection,
  getRepository,
} from 'typeorm';
import {
  AuthInput
} from './dto/auth.input';
import {
  Token
} from './token.entity';
import * as bcrypt from 'bcrypt'
 
import {
  env
} from 'process';
import * as zenvia from '@zenvia/sdk';
import {
  EmailConfirmation
} from 'src/email-confirmation/entities/email-confirmation.entity';
import * as nodemailer from 'nodemailer';
import { UserService } from 'src/user/user.service';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService
  ) {}

  async validateUser(data: AuthInput): Promise < any > {
    const user = await this.userService.getUserByEmail(data.email);
    const tokenRepository = getRepository(Token);
    const emailConfirmationRepository = getRepository(EmailConfirmation);

    if(!user)  {
      throw new UnauthorizedException('account not exist');
    }

    const validPasssword = await bcrypt.compare(data.password, user.password);
    if (!validPasssword) {
      throw new UnauthorizedException('Incorrect password');
    }

    // const content = new zenvia.TextContent('some text message here');


    const verifyRole = await getConnection().query(
      `select *
      from user_role ur
      where  ur."userId" = $1
      `, [user.id]
    )

    const selectRole = await getConnection().query(
      `SELECT *
        FROM role r
        WHERE r."id" = '${verifyRole[0].roleId}'`
    )

    var minutesToAdd = 30;
    var currentDate = new Date();
    var futureDate = new Date(currentDate.getTime() + minutesToAdd * 60000);

    // if (user.status == 'valid_email') {
    //   let code = Math.random().toString(36).substring(6);
    //   const newEmailConfirmation = emailConfirmationRepository.create({
    //     status: 'enabled',
    //     userId: user.id,
    //     expires: futureDate,
    //     confirmationCode: code,
    //     type: 'email'
    //   });

    //   await emailConfirmationRepository.save(newEmailConfirmation);
 
    //   const transporter = nodemailer.createTransport({
    //     host: env.SMTP_SERVER,
    //     port: 465,
    //     secure: true,  
    //     auth: {
    //       user: env.SMTP_USERNAME,
    //       pass: env.SMTP_PASSWORD
    //     },
    //     tls: {
    //       rejectUnauthorized: false
    //     }
    //   });

    //   const mailOptions = {
    //     from: 'contato@setytecnologia.com.br',
    //     to: user.email,
    //     subject: 'E-mail de confirmação VISTORIA',
    //     text: `Seu codigo de confirmação para o VISTORIA é: 
    // ${code}`
    //   };
  
    //   transporter.sendMail(mailOptions, function (error, info) {
    //     if (error) {
    //       console.log(error);
    //     } else {
    //       console.log('Email enviado: ' + info.messageId);
    //     }
    //   });

    //   throw new UnauthorizedException('Unauthorized, unconfirmed user, please verify your email');
 
    // }
    
    
    // if (user.status == 'valid_phone') {
 
   
    //   let code = Math.random().toString(36).substring(6);
    //   const newPhoneConfirmation = emailConfirmationRepository.create({
    //     status: 'enabled',
    //     userId: user.id,
    //     expires: futureDate,
    //     confirmationCode: code,
    //     type: 'phone'
    //   });

    //   await emailConfirmationRepository.save(newPhoneConfirmation);
   
    //    var content =   new zenvia.TextContent(`Seu codigo de confirmação para o VISTORIA é: ${code}`);
 
    //   const client = new Client(process.env.YOUR_API_TOKEN);

    //   const sms = client.getChannel('sms');
   
    //   const response = await sms.sendMessage('sety-web', `${user.phone}`, content)
    //   throw new UnauthorizedException('Unauthorized, unconfirmed user, please verify your phone');
    // }


    if (user.status == 'new') {
      throw new UnauthorizedException('Unauthorized, wait for approval from an adm');
    }


    const findUserOnToken = await tokenRepository.findOne({
      userId: `${user.id}`
    })



    const token = await this.jwtToken(user, verifyRole[0]);

    const dateNow: Date = new Date();
    dateNow.setMinutes(dateNow.getMinutes() + 5);

    const newToken = tokenRepository.create({
      token,
      tokenStatus: 'Active',
      userId: user.id,
      expires: dateNow,
    });

    await tokenRepository.save(newToken);

    return {
      token,
      user,
      newToken,
      selectRole
    };
  }

  private async jwtToken(user: User, userRole: UserRole): Promise < string > {
    const payload = {
      username: user.name,
      sub: user.id,
      role: userRole.id
    };
    return this.jwtService.signAsync(payload);
  }
}