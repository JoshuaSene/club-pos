import { Field, Int, ObjectType  } from '@nestjs/graphql';
import Role from 'src/role/entities/role.entity';  
import { UserDTO } from 'src/user/dto/user.dto';
import { User } from 'src/user/entities/user.entity';
// import { Token } from '../token.entity';

@ObjectType()
export class AuthType {
  //@Field(() => User)
  @Field(() => UserDTO)
  user: UserDTO;

  @Field(() => String)
  token: string;

  @Field(() => String)
  tokenStatus: string;

  @Field(() => String)
  userId: string;

  @Field(() => String)
  role: string;

  @Field(() => String)
  expires: string;
  newToken: AuthType | PromiseLike<AuthType>;
}
