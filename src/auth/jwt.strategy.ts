import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common'; 
import { getConnection, getRepository } from 'typeorm'; 
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';
import { UserRole } from 'src/user-role/entities/user-role.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET
    });
  }

  async validate(payload: { sub: User['id']; name: string; role: UserRole['id'] }) {
    const user = this.userService.getUserById(payload.sub);

    const usersRole = getRepository(UserRole)
   
    const selectUserRole = await getConnection().query(
      `SELECT *
      FROM user_role ur
      WHERE ur."id" = '${payload.role}'`
      ) 
    const selectRole = await getConnection().query(
      `SELECT *
      FROM role r
      WHERE r."id" = '${selectUserRole[0].id}'`
      ) 
    
    if (!user) {
      throw new UnauthorizedException('Unauthorized');
    }

    let data  = {}
    data['user'] =  user
    data['role'] = selectRole[0]
    data['userRole'] = selectUserRole[0]

    return  data
  }
}
