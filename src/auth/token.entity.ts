import { Field, ID, ObjectType } from '@nestjs/graphql';  
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

@ObjectType()
@Entity('token')
export class Token {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @Column()
  token: string;

  @Column()
  tokenStatus: string;

  @Column()
  expires: Date;

  @ManyToOne(() => User, () => User)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  userId: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
