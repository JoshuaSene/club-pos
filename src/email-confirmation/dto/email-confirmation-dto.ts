import { FilterableField } from '@nestjs-query/query-graphql';
import { GraphQLISODateTime, ID, ObjectType } from '@nestjs/graphql';
 

@ObjectType('EmailConfirmation')
export class EmailConfirmationDTO {
  @FilterableField(() => ID)
  id!: string;
 
  @FilterableField()
  email!: string;

  @FilterableField()
  confirmationCode!: string;
 
  @FilterableField() 
  status!: string;
 
  @FilterableField()
  expires!: string;
 
  @FilterableField()
  userId!: string;
  
  @FilterableField(() => GraphQLISODateTime)
  created_at!: Date;

  @FilterableField(() => GraphQLISODateTime)
  updated_at!: Date;
}