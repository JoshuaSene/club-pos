import { FilterableField } from '@nestjs-query/query-graphql';
import { InputType, Int, Field, ID } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';

@InputType()
export class UserConfirmationDTO {
  @FilterableField(() => ID)
  id!: string; 

  @FilterableField()
  userId: string;

  @FilterableField()
  email: string;
  
  @FilterableField()
  confirmationCode: string;

  @FilterableField()
  expires: string;

  @FilterableField()
  @IsOptional()
  status!: string;

  @FilterableField()
  @IsOptional()
  type!: string;
}
