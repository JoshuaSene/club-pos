 import {
   NestjsQueryGraphQLModule
 } from '@nestjs-query/query-graphql';
 import {
   NestjsQueryTypeOrmModule
 } from '@nestjs-query/query-typeorm';
 import {
   Module
 } from '@nestjs/common';
 import {
   EmailConfirmationDTO
 } from './dto/email-confirmation-dto';
 import {
   EmailConfirmationService
 } from './email-confirmation.service';
 import {
   EmailConfirmation
 } from './entities/email-confirmation.entity';

 @Module({
   imports: [
     NestjsQueryGraphQLModule.forFeature({
       imports: [NestjsQueryTypeOrmModule.forFeature([EmailConfirmation])],
       services: [EmailConfirmationService],
       resolvers: [{
         EntityClass: EmailConfirmation,
         DTOClass: EmailConfirmationDTO,
         // guards: [GqlAuthGuard],
         ServiceClass: EmailConfirmationService
       }]
     })
   ],
 })
 export class EmailConfirmationModule {}