import {
    QueryService
} from "@nestjs-query/core";
import {
    TypeOrmQueryService
} from "@nestjs-query/query-typeorm";
import {
    Injectable,
    InternalServerErrorException
} from "@nestjs/common";
import {
    InjectRepository
} from "@nestjs/typeorm";
import {
    Repository
} from "typeorm";
import {
    EmailConfirmationDTO
} from "./dto/email-confirmation-dto";
import {
    EmailConfirmation
} from "./entities/email-confirmation.entity";



@QueryService(EmailConfirmation)
@Injectable()
export class EmailConfirmationService extends TypeOrmQueryService < EmailConfirmation > {
    constructor(
        @InjectRepository(EmailConfirmation) private emailConfirmationRepository: Repository < EmailConfirmation >
    ) {
        super(emailConfirmationRepository);
    }
    async createOne(data: EmailConfirmationDTO): Promise < any > {



        let emailConfirmation = this.emailConfirmationRepository.create(data);
        var minutesToAdd = 30;
        var currentDate = new Date();
        var futureDate = new Date(currentDate.getTime() + minutesToAdd * 60000);
        emailConfirmation.confirmationCode = Math.random().toString(36).substring(6);
        emailConfirmation.expires = futureDate

        const saved = await this.emailConfirmationRepository.save(emailConfirmation);
        if (!saved) {
            throw new InternalServerErrorException('Problema para confirmar email');
        }
        return emailConfirmation;
    }
 
}