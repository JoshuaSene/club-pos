import { Args, Mutation, Resolver, Query } from "@nestjs/graphql";
import { EmailConfirmationDTO } from "./dto/email-confirmation-dto";
import { EmailConfirmationService } from "./email-confirmation.service";
import { EmailConfirmation } from "./entities/email-confirmation.entity";

Resolver('EmailConfirmation')
export class EmailConfirmationResolver {
  constructor(private emailConfirmationService: EmailConfirmationService) {}
 
  @Mutation(() => EmailConfirmationDTO)
  async createOne(@Args('data') data: EmailConfirmationDTO): Promise<EmailConfirmation> {

    const emailConfirmation = await this.emailConfirmationService.createOne(data);
     
    return emailConfirmation;
  }

 
  @Mutation(() => EmailConfirmation)
  async updateOne(
    @Args('id') email: string,
    @Args('data') data: EmailConfirmationDTO,
  ): Promise<EmailConfirmation> {
    
    return this.emailConfirmationService.updateOne(email, {...data});
  }
 
  // @Query(() => [EmailConfirmation])
  // async userByStatus(@Args('status') status: string): Promise<User[]> {
  //   const user = await this.userService.getUserByStatus(status);
  //   return user;
  // }
}
