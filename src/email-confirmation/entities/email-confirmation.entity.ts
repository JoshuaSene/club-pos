 

import { Field, ID, ObjectType } from '@nestjs/graphql';  
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

var minutesToAdd=30;
var currentDate = new Date();
var futureDate = new Date(currentDate.getTime() + minutesToAdd*60000); 

 
@ObjectType()
@Entity('emailConfirmation')
export class EmailConfirmation {
  @PrimaryGeneratedColumn('uuid')
  @Field(() => ID)
  id: string;

  @Column()
  confirmationCode: string;

  @Column({default: "valid_email"})
  status: string;

  @Column()
  type: string;

  @Column({default:futureDate})
  expires: Date;

  @ManyToOne(() => User, () => User)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  user: string;

  @Column({nullable: true})  
  userId: string;


  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
