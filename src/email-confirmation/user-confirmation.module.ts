import { Module } from '@nestjs/common';
  
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailConfirmation } from './entities/email-confirmation.entity';
import { UserConfirmationResolver } from './user-confirmation.resolver';
import { UserConfirmationService } from './user-confirmation.service';
 


@Module({
    imports: [TypeOrmModule.forFeature([EmailConfirmation])],
    providers: [UserConfirmationService,UserConfirmationResolver]
    
  })

  export class EnabledConfirmationModule {}