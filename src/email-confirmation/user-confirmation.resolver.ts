import {
  Args,
  Mutation,
  Resolver
} from "@nestjs/graphql";
import {
  UserConfirmationService
} from "./user-confirmation.service";


@Resolver('UserConfirmationDTO')
export class UserConfirmationResolver {
  constructor(
    private userConfirmationService: UserConfirmationService,
  ) {}

  @Mutation(() => String)
  async activeEmail(
    @Args('code') code: String,
    @Args('email') email: String
  ): Promise < any > {
    await this.userConfirmationService.getEmailConfirmation(code, email);
    return `Email ${email} has been registred`
  }

  @Mutation(() => String)
  async activePhone(
    @Args('code') code: String,
    @Args('phone') phone: String
  ): Promise < any > {
    await this.userConfirmationService.getPhoneConfirmation(code, phone);
    return `Phone ${phone} has been registred`
  }

  @Mutation(() => String)
  async forgetPassword(
    @Args('email') code: String,
    @Args('phone') phone: String,
    @Args('document') document: String,
  ): Promise < any > {
    await this.userConfirmationService.getPassword(code, phone, document);


    return `The password has been sent to your email`
  }
}