import {
  Injectable,
  UnauthorizedException
} from "@nestjs/common";
import {
  getConnection,
  getRepository,
  Repository
} from "typeorm";
import {
  InjectRepository
} from '@nestjs/typeorm';
import {
  EmailConfirmation
} from "./entities/email-confirmation.entity";
import {
  User
} from "src/user/entities/user.entity";
import {
  env
} from "process";
import * as bcrypt from 'bcrypt'
import * as nodemailer from 'nodemailer';

@Injectable()
export class UserConfirmationService {
  constructor(
    @InjectRepository(EmailConfirmation) private emailConfirmationRepository: Repository < EmailConfirmation > ,
  ) {}

  async getEmailConfirmation(code: String, email: String): Promise < any > {


    const user = await getRepository(User).findOne({
      email: `${email}`
    })
    if (!user) {
      throw new UnauthorizedException('the email is invalid')
    }

    const confirmation = await getConnection().query(
      `   
        select *
        from "emailConfirmation" ec     
        where  "confirmationCode" =$1
        and status = 'enabled'
         
        `, [code]
    )


    if ((confirmation.length < 1) || (user.id != confirmation[0].userId)) {
      throw new UnauthorizedException('the token is invalid')
    }

    if (confirmation[0].expires - new Date(Date()).getTime() <= 0) {
      throw new UnauthorizedException('the token has been expired   ')
    }

    const newStatus = {
      status: 'valid_phone'
    }

    const mergeUserStatus = getRepository(User).merge(user, newStatus)
    await getRepository(User).save(mergeUserStatus);
    return confirmation;



  }





  async getPhoneConfirmation(code: String, phone: String): Promise < any > {

    const user = await getRepository(User).findOne({
      phone: `${phone}`
    })

    if (!user) {
      throw new UnauthorizedException('the phone is invalid')
    }

    const confirmation = await getConnection().query(
      `   
           select *
           from "emailConfirmation" ec     
           where  "confirmationCode" =$1
           and status = 'enabled'
           and type = 'phone'
            
           `, [code]
    )

    if ((confirmation.length < 1) || (user.id != confirmation[0].userId)) {
      throw new UnauthorizedException('the token is invalid')
    }

    if (confirmation[0].expires - new Date(Date()).getTime() <= 0) {
      throw new UnauthorizedException('the token has been expired   ')
    }

    const newStatus = {
      status: 'new'
    }

    const mergeUserStatus = await getRepository(User).merge(user, newStatus)
    await getRepository(User).save(mergeUserStatus);
    return confirmation;



  }


  async getPassword(email: String, phone: String, document: String): Promise < any > {
    const user = await getRepository(User).findOne({
      phone: `${phone}` ,
      email: `${email}`
    })


    let newPassword = Math.random().toString(36).substring(6);

    const salt = await bcrypt.genSalt(10);

    const hash = await bcrypt.hash(newPassword, salt);


    const userUpdate = {
      password: hash
    }

    const userRepository = getRepository(User)
    const mergeUser = userRepository.merge(user, userUpdate);
    await userRepository.save(mergeUser);

    if (!user) {
      throw new UnauthorizedException('Invalid data')
    }

    const transporter = nodemailer.createTransport({
      host: env.SMTP_SERVER,
      port: 465,
      secure: true,
      auth: {
        user: env.SMTP_USERNAME,
        pass: env.SMTP_PASSWORD
      },
      tls: {
        rejectUnauthorized: false
      }
    });

    const mailOptions = {
      from: 'contato@setytecnologia.com.br',
      to: user.email,
      subject: 'E-mail de confirmação VISTORIA',
      text: `Codigo de confirmação: 
            ${newPassword}
         `
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email enviado: ' + info.messageId);
      }
    });

  }
}