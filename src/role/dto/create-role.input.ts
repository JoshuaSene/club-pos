import { InputType, Int, Field } from '@nestjs/graphql';
import Role from '../entities/role.entity';

@InputType()
export class CreateRoleInput {
  @Field(() => Role)
  name: string;
}
