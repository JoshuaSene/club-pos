import { FilterableField } from '@nestjs-query/query-graphql';
import { GraphQLISODateTime, ID, ObjectType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty } from 'class-validator';
@ObjectType('Role')
export class RoleDTO {
  @FilterableField(() => ID)
  id!: string;

  @IsNotEmpty()
  @FilterableField()
  name!: string;
  
  @FilterableField(() => GraphQLISODateTime)
  created_at!: Date;

  @FilterableField(() => GraphQLISODateTime)
  updated_at!: Date;
}