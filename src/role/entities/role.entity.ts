 
import { UserRole } from 'src/user-role/entities/user-role.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
  JoinTable,
} from 'typeorm';
 
@Entity('role')
class Role {
  @PrimaryGeneratedColumn('uuid') 
  id: string;

  @OneToMany(() => UserRole, (userRole) => userRole.role)
  @JoinTable()
  userRole!: UserRole[];

  @Column() 
  name: string;

  @CreateDateColumn()
  created_at: Date;

  @CreateDateColumn()
  updated_at: Date;
}
export default Role;
