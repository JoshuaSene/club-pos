import { Module } from '@nestjs/common';
import Role from './entities/role.entity';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { RoleDTO } from './dto/role.dto';
import { GqlAuthGuard, GuardByRole } from 'src/auth/auth.guard';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Role])],
      resolvers: [
        {
          EntityClass: Role,
          DTOClass: RoleDTO,
          guards: [GqlAuthGuard],
          create: { guards: [GuardByRole] },
          update: {guards: [GuardByRole] } ,
          delete: {disabled: true},  
        }
      ]
    })
  ], 
})
export class RoleModule {}
