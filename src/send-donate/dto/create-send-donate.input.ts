import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateSendDonateInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
