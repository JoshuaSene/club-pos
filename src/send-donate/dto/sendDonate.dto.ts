import { FilterableField } from '@nestjs-query/query-graphql';
import { GraphQLISODateTime, ID, ObjectType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty } from 'class-validator';
@ObjectType('SendDonate')
export class SendDonateDTO {
  @FilterableField(() => ID)
  id!: string;

  @IsNotEmpty()
  @FilterableField()
  name!: string;
  
  @FilterableField(() => GraphQLISODateTime)
  created_at!: Date;

  @FilterableField(() => GraphQLISODateTime)
  updated_at!: Date;
}