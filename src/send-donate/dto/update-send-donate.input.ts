import { CreateSendDonateInput } from './create-send-donate.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateSendDonateInput extends PartialType(CreateSendDonateInput) {
  @Field(() => Int)
  id: number;
}
