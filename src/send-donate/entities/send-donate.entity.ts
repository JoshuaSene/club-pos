import { UserRole } from 'src/user-role/entities/user-role.entity';
import { User } from 'src/user/entities/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';
 
@Entity('sendDonate')
class SendDonate {
  @PrimaryGeneratedColumn('uuid') 
  id: string;

  @Column({ nullable: false })
  userId!: string;

  @ManyToOne(() => User, (u) => u.sendDonate, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  donate!: SendDonate;
  
  
  @Column() 
  status: string;
  
  @Column()
  value: number

  @CreateDateColumn()
  created_at: Date;

  @CreateDateColumn()
  updated_at: Date;
}
export default SendDonate;
