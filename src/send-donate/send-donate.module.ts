import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Module } from '@nestjs/common';
import { GuardByRole } from 'src/auth/auth.guard';
import { SendDonateDTO } from './dto/sendDonate.dto';
import SendDonate from './entities/send-donate.entity';
  

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([SendDonate])],
      resolvers: [{
        EntityClass: SendDonate,
        DTOClass: SendDonateDTO,
        // guards: [GqlAuthGuard ],
        // read: { guards: [GuardByRole] },
        // create: { guards: [GuardByRole] },
        update: {
          guards: [GuardByRole]
        },
        delete: {
          disabled: true
        }
      }]
    })
  ]
})
export class SendDonateModule {}