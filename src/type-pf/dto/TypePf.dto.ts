import {
    FilterableField
} from '@nestjs-query/query-graphql';
import {
    GraphQLISODateTime,
    ID,
    ObjectType
} from '@nestjs/graphql';
import {
    IsEmail,
    IsNotEmpty
} from 'class-validator';
@ObjectType('TypePF')
export class TypePFDTO {
    @FilterableField(() => ID)
    id!: string;

    @IsNotEmpty()
    @FilterableField()
    name!: string;

    @FilterableField()
    userId: string

    @FilterableField()
    adressComplement: string

    @FilterableField()
    adressZipCode: string

    @FilterableField()
    adressNumber: string

    @FilterableField()
    phone: string

    @FilterableField()
    addressCountry: string

    @FilterableField()
    addressCity: string

    @FilterableField()
    addressDistrict: string

    @FilterableField()
    addressStreet: string

    @FilterableField()
    cpf: string

    @FilterableField()
    docucmentIssuer: string

    @FilterableField()
    documentType: string

    @FilterableField()
    documentImage: string

    @FilterableField()
    documentNumber: string

    @FilterableField()
    profession: string

    @FilterableField()
    motherName: string

    @FilterableField()
    fatherName: string

    @FilterableField()
    status: string

    @FilterableField()
    rg: string
 
    @FilterableField(() => GraphQLISODateTime)
    created_at!: Date;

    @FilterableField(() => GraphQLISODateTime)
    updated_at!: Date;
}