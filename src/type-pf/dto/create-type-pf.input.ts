import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateTypePfInput {
  @Field(() => Int, { description: 'Example field (placeholder)' })
  exampleField: number;
}
