import { CreateTypePfInput } from './create-type-pf.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateTypePfInput extends PartialType(CreateTypePfInput) {
  @Field(() => Int)
  id: number;
}
