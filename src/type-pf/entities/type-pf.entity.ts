 
import SendDonate from 'src/send-donate/entities/send-donate.entity';
import {
  UserRole
} from 'src/user-role/entities/user-role.entity';
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToOne,
  ObjectType,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn
} from 'typeorm';

 
@Entity({
  name: 'TYPE_PF'
})
export class TypePf {
  @PrimaryGeneratedColumn('uuid')
  id: string;
 
  @Column({ nullable: false })
  userId!: string;

  @ManyToOne(() => User, (u) => u.typePf, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  user!: User;
 
  @Column({
    nullable: false
  })
  name: string;

  @Column({
    nullable: false
  })
  contractAdhesion: string;

  @Column({
    nullable: false
  })
  rg: string;

  @Column({
    nullable: false
  })
  status: string;

  @Column({
    nullable: false
  })
  fatherName: string;

  @Column({
    nullable: false
  })
  motherName: string;

  @Column({
    nullable: false
  })
  profession: string;

  @Column({
    nullable: false
  })
  documentNumber: string;


  @Column({
    nullable: false
  })
  documentImage: string;

  @Column({
    nullable: false
  })
  documentType: string;

  @Column({
    nullable: false
  })
  docucmentIssuer: string;


  @Column({
    nullable: false,
    unique: true
  })
  cpf: string;

  @Column({
    nullable: false ,
    unique: true
  })
  addressStreet: string;

  @Column({
    nullable: false 
  })
  addressDistrict: string;


  @Column({
    nullable: false 
  })
  addressCity: string;

  @Column({
    nullable: false 
  })
  addressCountry: string;

  @Column({
    unique: true,
    nullable: false
  })
  phone: string;

  @Column({
    nullable: false 
  })
  adressNumber: string;

  @Column({
    nullable: false 
  })
  adressZipCode: string;


  @Column({
    nullable: false 
  })
  adressComplement: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}