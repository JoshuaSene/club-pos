import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Module } from '@nestjs/common';
import { GuardByRole } from 'src/auth/auth.guard';
import { TypePFDTO } from './dto/TypePf.dto';
import { TypePf } from './entities/type-pf.entity';
 
@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([TypePf])],
      resolvers: [{
        EntityClass: TypePf,
        DTOClass: TypePFDTO,
        // guards: [GqlAuthGuard ],
        // read: { guards: [GuardByRole] },
        // create: { guards: [GuardByRole] },
        update: {
          guards: [GuardByRole]
        },
        delete: {
          disabled: true
        }
      }]
    })
  ]
})
export class TypePfModule {}