import { CreateTypePjInput } from './create-type-pj.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateTypePjInput extends PartialType(CreateTypePjInput) {
  @Field(() => Int)
  id: number;
}
