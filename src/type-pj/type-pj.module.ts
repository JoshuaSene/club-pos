import { Module } from '@nestjs/common';
import { TypePjService } from './type-pj.service';
import { TypePjResolver } from './type-pj.resolver';

@Module({
  providers: [TypePjResolver, TypePjService]
})
export class TypePjModule {}
