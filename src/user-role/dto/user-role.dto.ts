import {
  FilterableField,
  Relation
} from '@nestjs-query/query-graphql';
import {
  GraphQLISODateTime,
  ID,
  ObjectType
} from '@nestjs/graphql';
import {
  RoleDTO
} from 'src/role/dto/role.dto';
import {
  UserDTO
} from 'src/user/dto/user.dto';


@ObjectType('UserRole')
@Relation('Role', () => RoleDTO, {
  disableRemove: true
})
@Relation('User', () => UserDTO, {
  disableRemove: true
})
export class UserRoleDTO {

  @FilterableField(() => ID)
  id!: string;

  @FilterableField()
  roleId!: string;

  @FilterableField()
  userId!: string;

  @FilterableField(() => GraphQLISODateTime)
  created_at!: Date;

  @FilterableField(() => GraphQLISODateTime)
  updated_at!: Date;
}