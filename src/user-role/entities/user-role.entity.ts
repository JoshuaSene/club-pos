import Role from 'src/role/entities/role.entity'; 
import { User } from 'src/user/entities/user.entity';
 
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  ObjectType
}   from 'typeorm';
 
@Entity('user_role')
export class UserRole {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  //@ManyToOne((type) => Role, () => UserRole)
  //@JoinColumn({ name: 'roleId', referencedColumnName: 'id' })
  //roleId: string;

  @Column({ nullable: false })
  userId!: string;

  @ManyToOne(() => User, (u) => u.userRole, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  user!: User;

  @Column({ nullable: false })
  roleId!: string;

  @ManyToOne(() => Role, (r) => r.userRole, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  role!: Role;
  
  // @ManyToOne(
  //   (): ObjectType < User > => User,
  //   rm => rm.userRole, {
  //     onDelete: 'CASCADE',
  //     nullable: false
  //   },
  // )
  // @JoinColumn()
  // user!: User;

  // @Column()
  // @FilterableField()
  // usersId: string
 
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
