 import {
   Module
 } from '@nestjs/common';
 import {
   NestjsQueryGraphQLModule
 } from '@nestjs-query/query-graphql';
 import {
   NestjsQueryTypeOrmModule
 } from '@nestjs-query/query-typeorm';
 import {
   GqlAuthGuard,
   GuardByRole,
   RolesGuard
 } from 'src/auth/auth.guard';
 import {
   UserRole
 } from './entities/user-role.entity';
 import {
   UserRoleDTO
 } from './dto/user-role.dto';

 @Module({
   imports: [
     NestjsQueryGraphQLModule.forFeature({
       imports: [NestjsQueryTypeOrmModule.forFeature([UserRole])],
       resolvers: [{
         EntityClass: UserRole,
         DTOClass: UserRoleDTO,
         // guards: [GqlAuthGuard ],
         // read: { guards: [GuardByRole] },
         // create: { guards: [GuardByRole] },
         update: {
           guards: [GuardByRole]
         },
         delete: {
           disabled: true
         }
       }]
     })
   ]
 })
 export class UserRoleModule {}