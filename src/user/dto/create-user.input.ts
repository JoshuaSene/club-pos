import { InputType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';

@InputType()
export class CreateUserInput {
  @IsString()
  @IsNotEmpty({ message: 'This field is required' })
  name: string;

  @IsEmail()
  @IsNotEmpty({ message: 'This field is required' })
  email: string;
   
  @IsNotEmpty({ message: 'This field is required' })
  companyCNPJ: string;
 
  companyId: string;
  
  @IsNotEmpty({ message: 'This field is required' })
  phone: string;

  @IsString()
  @IsNotEmpty({ message: 'This field is required' })
  document: string;

  @IsString()
  @IsNotEmpty({ message: 'Password is required' })
  password?: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty({ message: 'Password is required' })
  status: string;
}
