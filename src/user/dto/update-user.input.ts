import { CreateUserInput } from './create-user.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';
import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';

@InputType()
export class UpdateUserInput extends PartialType(CreateUserInput) {
  @IsString()
  @IsNotEmpty({ message: 'Invalid characters' })
  @IsOptional()
  name?: string;

  id: string 
  
  @IsEmail()
  @IsNotEmpty({ message: 'Invalid E-mail' })
  @IsOptional()
  email?: string;

  
  @IsNotEmpty({ message: 'Invalid phone' })
  @IsOptional()
  phone?: string;

  @IsString()
  @IsNotEmpty({ message: 'This field is required' })
  @IsOptional()
  document?: string;

  @IsOptional()
  @IsString()
  // @IsNotEmpty({ message: 'Password is required' })
  password?: string;

  @FilterableField()
  @IsOptional()
  @IsString()
  // @IsNotEmpty({ message: 'Password is required' })
  status?: string;
}
