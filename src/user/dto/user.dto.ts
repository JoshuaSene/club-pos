import { FilterableField } from '@nestjs-query/query-graphql';
import { GraphQLISODateTime, ID, ObjectType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, } from 'class-validator';



@ObjectType('User')
export class UserDTO {
  @FilterableField(() => ID)
  id!: string;

  // @IsNotEmpty()
  @FilterableField()
  name!: string;
 
  @FilterableField()
  @IsEmail()
  email!: string;

  @FilterableField()
  companyCNPJ: string;

  // @IsNotEmpty()
  @FilterableField()
  password!: string;

  @FilterableField()
  phone!: string;
  
  // @IsNotEmpty()
  @FilterableField()
  file!: string;


  // @IsNotEmpty()
  @FilterableField()
  status: string;

  // @IsNotEmpty()
  @FilterableField()
  realtor!: string;
 
  // @IsNotEmpty()
  @FilterableField()
  document: string;
  
  @FilterableField(() => GraphQLISODateTime)
  created_at!: Date;

  @FilterableField(() => GraphQLISODateTime)
  updated_at!: Date;
}