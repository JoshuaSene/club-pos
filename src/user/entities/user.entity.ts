 
 import {
   EmailConfirmation
 } from 'src/email-confirmation/entities/email-confirmation.entity'; 
import SendDonate from 'src/send-donate/entities/send-donate.entity';
import { TypePf } from 'src/type-pf/entities/type-pf.entity';
 import {
   UserRole
 } from 'src/user-role/entities/user-role.entity';
 import {
   Column,
   CreateDateColumn,
   Entity, 
   JoinTable, 
   OneToMany, 
   PrimaryGeneratedColumn, 
   UpdateDateColumn
 } from 'typeorm';

 enum StatusEnum {
   Active = 'active',
     Disabled = 'disabled',
     New = 'new'
 }

 @Entity({
   name: 'user'
 })
 export class User {
   @PrimaryGeneratedColumn('uuid')
   id: string;

   @OneToMany(() => UserRole, (userRole) => userRole.user)
   @JoinTable()
   userRole!: UserRole[];

   @OneToMany(() => SendDonate, (sendDonate) => sendDonate.id)
   @JoinTable()
   sendDonate!: SendDonate[];

   @OneToMany(() => User, (user) => user.user)
   @JoinTable()
   user!: User[];

   @OneToMany(() => TypePf, (typePf) => typePf.user)
   @JoinTable()
   typePf!: User[];
   
   @OneToMany(() => TypePf, (typePj) => typePj.user)
   @JoinTable()
   typePj!: User[];
   
   @OneToMany(() => EmailConfirmation, (ec) => ec.userId)
   @JoinTable()
   userEC!: User[];

  
   @Column({
     nullable: false
   })
   name: string;

   @Column({
     nullable: false,
     default: 'valid_email'
   })
   status: string;
 
 
   @Column({
     unique: true,
     nullable: false
   })
   email: string;
 
   @Column({
     nullable: false
   })
   password: string;

   @Column({
     unique: true,
     nullable: false
   })
   phone: string;

   @CreateDateColumn()
   created_at: Date;

   @UpdateDateColumn()
   updated_at: Date;
 }