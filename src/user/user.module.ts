 

import { Module } from '@nestjs/common';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import {  GqlAuthGuard } from 'src/auth/auth.guard'; 
import { User } from './entities/user.entity';
import { UserDTO } from './dto/user.dto';
import { UserService } from './user.service'; 
 
@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([User])],
      services: [UserService],
      resolvers: [
        {
          EntityClass: User,
          DTOClass: UserDTO,
          ServiceClass: UserService, 
          read: {guards: [GqlAuthGuard]},
          update: {guards: [GqlAuthGuard]}, 
          // update: {guards: [ GqlAuthGuard,  GuardByRole] } ,
          delete: {disabled: true},  
        }
      ]
    })
  ], 
})
export class UserModule {}
