import {
  Args,
  Mutation,
  Query,
  Resolver
} from '@nestjs/graphql';
import {
  CreateUserInput
} from './dto/create-user.input';
import {
  UpdateUserInput
} from './dto/update-user.input';
import {
  UserService
} from './user.service';
import {
  GqlAuthGuard,
  RolesGuard
} from './../auth/auth.guard';
import {
  Request,
  UseGuards
} from '@nestjs/common';
import {
  User
} from './entities/user.entity';

@Resolver('User')
export class UserResolver {
  constructor(private userService: UserService) {}

  // @UseGuards(RolesGuard)
  @UseGuards(GqlAuthGuard)
  @Query(() => [User])
  async users(@Request() req): Promise < User[] > {
    const users = await this.userService.findAllUsers();
    return users;
  }

  // @UseGuards(GqlAuthGuard)
  // @Query(() => [User])
  // async usersEnabled(@Request() req): Promise<User[]> {
  //   const users = await this.userService.findEnabledUsers();
  //   return users;
  // }

  @UseGuards(GqlAuthGuard)
  @Query(() => User)
  async user(@Args('id') id: string): Promise < User > {
    const user = await this.userService.getUserById(id);
    return user;
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => [User])
  async userByStatus(@Args('status') status: string): Promise < User[] > {
    const user = await this.userService.getUserByStatus(status);
    return user;
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => User)
  async userByEmail(@Args('email') email: string): Promise < User > {
    const user = await this.userService.getUserByEmail(email);
    return user;
  }

  @Mutation(() => User)
  async createOne(@Args('data') data: CreateUserInput): Promise < User > {
    const user = await this.userService.createOne(data);
    return user;
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => User)
  async updateOne(
    @Args('id') id: string,
    @Args('data') data: UpdateUserInput,
  ): Promise < User > {

    return this.userService.updateOne(id, {
      ...data
    });
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => Boolean)
  async deleteUser(@Args('id') id: string): Promise < boolean > {
    const deleted = await this.userService.deleteUser(id);
    return deleted;
  }
}