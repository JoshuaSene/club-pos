import {
  Injectable,
  InternalServerErrorException,
  NotFoundException
} from '@nestjs/common';
import {
  InjectRepository
} from '@nestjs/typeorm';
import {
  getConnection,
  getRepository,
  Repository
} from 'typeorm';
import {
  CreateUserInput
} from './dto/create-user.input';
import {
  UpdateUserInput
} from './dto/update-user.input';
import * as UserRole from './../user-role/entities/user-role.entity'

import {
  User
} from './entities/user.entity';
import {
  TypeOrmQueryService
} from '@nestjs-query/query-typeorm';
import {
  QueryService
} from '@nestjs-query/core';
import * as bcrypt from 'bcrypt';
 

@QueryService(User)
@Injectable()
export class UserService extends TypeOrmQueryService < User > {
  constructor(
    @InjectRepository(User) private userRepository: Repository < User > ,
  ) {
    super(userRepository)
  }

  async findAllUsers(): Promise < User[] > {
    const users = await this.userRepository.find();
    return users;
  }


  async getUserById(data: string): Promise < User > {
    const user = await this.userRepository.findOne(data);
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async getUserByEmail(email: string): Promise < User > {
    const user = this.userRepository.findOne({
      where: {
        email
      }
    });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async getUserByStatus(status: string): Promise < User[] > {
    const user = this.userRepository.find({
      where: {
        status
      }
    });
    if (!user) {
      throw new NotFoundException('User not found');
    }

    return user;
  }

  async createOne(data: CreateUserInput): Promise < User > {
    const email = data.email;
    const checkUserExists = await getRepository(User).findOne({
      where: {
        email
      }
    });


    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(data.password, salt);
    data.password = hash;

    
    if (checkUserExists) {
      throw new InternalServerErrorException('Email address already used');
    }
    const user = this.userRepository.create(data);
    const userSaved = await this.userRepository.save(user);
    const userRoleRepository = getRepository(UserRole.UserRole);

    const selectRole = await getConnection().query(
      `SELECT *
        FROM role r
        WHERE name = 'USER'`
    );

    await userRoleRepository.insert({
      roleId: selectRole[0].id,
      userId: userSaved.id
    });

    //  await userRoleRepository.save(userRoles);

    if (!userSaved) {
      throw new InternalServerErrorException('Problem creating a user');
    }

    return userSaved;
  }

  async updateOne(id: string, data: UpdateUserInput): Promise < User > {
    let user = await this.getUserById(id);
    const password = data.password
    if (password) {
      const salt = await bcrypt.genSalt(10);

      const hash = await bcrypt.hash(data.password, salt);
      data.password = hash;
      const result = this.userRepository.save({
        ...user,
        ...data
      });
      return result
    }

    const result = this.userRepository.save({
      ...user,
      ...data
    });
    return result

  }

  async deleteUser(id: string): Promise < boolean > {
    const user = await this.getUserById(id);
    const deleted = await this.userRepository.delete(user);

    if (deleted) {
      return true;
    }
    return false;
  }
}